//unsigned int fact(unsigned int n) - Calculates and returns the factorial of n

#include<stdio.h>
#include<math.h>

unsigned int fact(unsigned int n);

int main ()
{
	unsigned int n;
	printf("Enter the positive number: ");
	scanf("%u",&n);
	
	unsigned int f = fact(n);
	
	printf("Factorial of %u = %u\n",n,f);
	
}

unsigned int fact(unsigned int n)
{
	unsigned long long  factorial = 1;
	
	for (int i=1; i<=n; i++)
	{
		factorial *= i;
	}
	return factorial;
}

/*
OUTPUT:



*/