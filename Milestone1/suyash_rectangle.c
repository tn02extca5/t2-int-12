//A program to calculate the Area of a rectangle

#include<stdio.h>

void main ()
{
	
	float l,b,area;
	printf("Enter length in meter:");
	scanf("%f", &l);
	printf("Enter breadth in meter:");
	scanf("%f", &b);
	area= l*b;
	printf("area of rectangle is %f sq.meter" , area );
	
}
/*
Output

C:\Users\Student\Desktop\c course>gcc rectangle.c

C:\Users\Student\Desktop\c course>a.exe
Enter length in meter:10
Enter breadth in meter:20
area of rectangle is 200.000000 sq.meter


*/