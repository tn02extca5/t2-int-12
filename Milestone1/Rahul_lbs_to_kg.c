//A program to convert weight entered in pounds(lbs) to kilogram(kg)

#include<stdio.h>
float main()
{
	float lbs,kg;
	printf("Enter Weight in lbs: ");
	scanf("%f",&lbs);
	kg=lbs*0.4536;
	printf("Weight in kgs= %f",kg);
}


/*
OUTPUT:

c:\Mycode>gcc lbs_to_kg.c

c:\Mycode>a.exe
Enter Weight in lbs: 25
Weight in kgs= 11.340000
c:\Mycode>

*/