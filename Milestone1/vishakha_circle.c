//Program to calculate Area of circle

#include<stdio.h>

void main()
{
	float r,area,pi;
	pi=3.142;
	printf("Enter the radius of circle: \n");
	scanf("%f",&r);
	area = pi*r*r;
	printf("Area of Circle  is = %f \n",area);
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_circle.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter the radius of circle:
5
Area of Circle  is = 78.549995

*/