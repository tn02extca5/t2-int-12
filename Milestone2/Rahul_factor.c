//Function for finding and printing all factors of a number

#include<stdio.h>
#include<math.h>

int factor(int n);

int main ()
{
	int n;
	printf("Enter the number: ");
	scanf("%d", &n);
	
	factor(n);
}

int factor(int n)
{
	int i;
	printf("Factors of the given number (%d):\n",n);
	for(i=1; i <= n; ++i)
    {
        if (n%i == 0)
        {
			printf("%d  ",i);
        }
    }
	printf("\n");
}

/*
OUTPUT:

c:\Mycode>gcc factor.c

c:\Mycode>a.exe
Enter the number: 20
Factors of the given number (20):
1  2  4  5  10  20

c:\Mycode>

*/