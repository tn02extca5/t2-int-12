//A program to calculate the Area of a Rectangle

#include<stdio.h>
float main()
{
	float l,w,a;
	printf("Enter lenght of rectangle: ");
	scanf("%f",&l);
	printf("Enter width of rectangle: ");
	scanf("%f",&w);
	a=l*w;
	printf("Area of rectangle: %.02f",a);
}

/*
OUTPUT:

c:\Mycode>gcc areaofrectangle.c

c:\Mycode>a.exe
Enter lenght of rectangle: 5
Enter width of rectangle: 7
Area of rectangle: 35.00
c:\Mycode>

*/