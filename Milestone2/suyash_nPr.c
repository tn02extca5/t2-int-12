//nPr - Permutations

#include<stdio.h>
#include<math.h>

int fact(int n);
 
int main()
{
    int n,r,nPr;
	printf("Enter a number n: ");
  	scanf("%d",&n);
    	printf("Enter a number r: ");
  	scanf("%d",&r);
  	
	nPr=fact(n)/fact(n-r);
    printf("Value of %dP%d (nPr)= %d\n",n,r,nPr);
}
 
int fact(int n)
{
    int i,f = 1;
    for(i=1 ; i<=n ; i++)
    {
        f *= i;
    }
    return f;
}

/*
OUTPUT:


*/