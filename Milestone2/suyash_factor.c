// program to find factors of a number


#include<stdio.h>
#include<conio.h>
int factor (int num);
void main ()
{
	int num;
	printf("Enter a number : ");
	scanf(" %d",&num);
	factor(num);
}	
		
	int factor (int num)
	{
		int i;
	printf("Factors of %d  number are :", num );
	for( i=1; i<=num ; ++i)
	{
		if(num%i==0)
		{
			printf("%d\t", i);
		}	
	}
	}


/*
output:


C:\Suyash>gcc factor.c

C:\Suyash>a.exe
Enter a number : 50
Factors of 50  number are :1    2       5       10      25      50
C:\Suyash>
*/