//A function to check if a number is odd or even

#include<stdio.h>
#include<math.h>

int even_or_odd(int n);

int main ()
{
	int n;
	printf("Enter the number: ");
	scanf("%d",&n);
	
	even_or_odd(n);
	
}

int even_or_odd(int n)
{
	if (n%2 == 0)
	{
		printf("%d is even\n",n);
	}
	else
	{
		printf("%d is odd\n",n );
	}
}

/*
OUTPUT:

C:\Users\Vishnu\Desktop\c>gcc vishakha_even_or_odd.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter the number: 65
65 is odd


*/