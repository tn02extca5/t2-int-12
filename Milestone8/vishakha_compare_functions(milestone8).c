#include "headers.h"

int str_compare(char *str1, char *str2);

int compare_int(void *a,void *b){

	int new_a  = *((int*)a);
	int new_b  = *((int*)b);

	if(new_a > new_b)
		return 1;
	else if(new_a < new_b)
		return -1;
	else
		return 0;
} 

int compare_float(void *a,void *b){

	float new_a  = *((float*)a);
	float new_b  = *((float*)b);

	if(new_a > new_b)
		return 1;
	else if(new_a < new_b)
		return -1;
	else
		return 0;
} 

int compare_char(void *a,void *b){

	char new_a  = *((char*)a);
	char new_b  = *((char*)b);

	if(new_a > new_b)
		return 1;
	else if(new_a < new_b)
		return -1;
	else
		return 0;
} 

int compare_string(void *a,void *b){

	char **new_a  = ((char**)a);
	char **new_b  = ((char**)b);

	return str_compare(*new_a,*new_b);

}

int str_compare(char *str1, char *str2){

	

	while(*str1 == *str2 && (*str1 != '\0' && *str2 != '\0')){
		str1++;
		str2++;
}
	
	if(*(str1)-*(str2) > 0)
		return 1;
	else if(*(str1)-*(str2) < 0)
		return -1;
	else
		return 0;

}