//A program to convert temperature in Farenheit(F) to Celcius(C)

#include<stdio.h>

void main ()
{
	
	float c,f;
	printf("Enter temperature in Farenheit:");
	scanf("%f", &f);
	c=(f-32)*5/9;
	printf("Temperature in celcius :%f C",c );
	
}

/*
Output

C:\Users\Student\Desktop\c course>gcc temp.c

C:\Users\Student\Desktop\c course>a.exe
Enter temperature in Farenheit:10
Temperature in celcius :-12.222222 C
C:\Users\Student\Desktop\c course>

*/