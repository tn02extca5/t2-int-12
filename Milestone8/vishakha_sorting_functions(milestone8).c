#include "headers.h"
#include <stdio.h>
#include "compare_functions.c"

void mem_copy(void *dest,void *src, int n);

void sort(void *A,int size,int n ,int (*compare)(void*,void*),int flag){
	int i,j;
	char *new_A=(char*)A;
	char *temp = (char*)malloc(sizeof(size));

	for(i = 0; i < n; i++){
	for(j=i+1;j < n;j++){

			if(flag == 1){

		if(compare((new_A+(i*size)),(new_A+(j*size))) > 0){
			mem_copy(temp,(new_A+(j*size)),size);
			mem_copy((new_A+(j*size)),(new_A+(i*size)),size);
			mem_copy((new_A+(i*size)),temp,size);
}
}

else if (flag == 2){
	
		if(compare((new_A+(i*size)),(new_A+(j*size))) < 0){
			mem_copy(temp,(new_A+(j*size)),size);
			mem_copy((new_A+(j*size)),(new_A+(i*size)),size);
			mem_copy((new_A+(i*size)),temp,size);
}
}

	}
}


	
}

void mem_copy(void *dest,void *src,  int n){
	int i;
	
	char *dest1 = (char*)dest;
	char *src1 = (char*)src;


	 for (i=0; i<n; i++) {
		if (dest1+i==src1 || src+i==dest1){
			return;
			}
	}
		

	for(i = 0;i < n; i++){
	dest1[i] = src1[i];
}


}

void int_sorting(int n){
	int A[MAX];
	int (*compare_ptr)(void*,void*) = compare_int;
	int i,choice;
	
				printf("Enter %d integers\n",n );

				for (i = 0; i < n; i++){
					scanf("%d",&A[i]);
				}

				printf("Your array is :: \n");
				for (i = 0; i < n; i++){
					printf("%d ",A[i]);
				}

				printf("\nPress <1> to sort in ascending order & <2> for descending order :: ");
				scanf("%d",&choice);

				sort(A,sizeof(int),n,compare_ptr,choice);

				printf("Sorted array is :: \n");

				for(i = 0; i < n; i++)
				printf("%d ",A[i]);
				
			getch();


}


void float_sorting(int n){
	float A[MAX];
	int (*compare_ptr)(void*,void*) = compare_float;
	int i,choice;
	
				printf("Enter %d floats\n",n );

				for (i = 0; i < n; i++){
					scanf("%f",&A[i]);
				}

				printf("Your array is :: \n");
				for (i = 0; i < n; i++){
					printf("%f ",A[i]);
				}

				printf("\nPress <1> to sort in ascending order & <2> for descending order :: ");
				scanf("%d",&choice);

				sort(A,sizeof(float),n,compare_ptr,choice);

				printf("Sorted array is :: \n");

				for(i = 0; i < n; i++)
				printf("%f ",A[i]);
				
			getch();


}

void char_sorting(int n){
	char A[MAX];
	int (*compare_ptr)(void*,void*) = compare_char;
	int i,choice;
	
				printf("Enter %d characters\n",n );

				for (i = 0; i < n; i++){
					scanf(" %c",&A[i]);
				}

				printf("Your array is :: \n");
				for (i = 0; i < n; i++){
					printf("%c ",A[i]);
				}

				printf("\nPress <1> to sort in ascending order & <2> for descending order :: ");
				scanf("%d",&choice);

				sort(A,sizeof(char),n,compare_ptr,choice);

				printf("Sorted array is :: \n");

				for(i = 0; i < n; i++)
				printf("%c ",A[i]);
				
			getch();


}

void string_sorting(int n){
	
	int (*compare_ptr)(void*,void*) = compare_string;
	int i,choice;
	char **A = (char**)malloc(sizeof(char*));
		for (i = 0; i < n; i++){
				A[i] = (char*)malloc(MAX*sizeof(char));
				}

				printf("Enter %d strings\n",n );

				for (i = 0; i < n; i++){
					scanf(" %[^\n]s",A[i]);
				}

				printf("Your strings are :: \n");
				for (i = 0; i < n; i++){
					printf("%s\n",A[i]);
				}

				printf("\nPress <1> to sort in ascending order & <2> for descending order :: ");
				scanf("%d",&choice);

				sort(A,sizeof(char*),n,compare_ptr,choice);

				printf("Sorted array is :: \n");

				for(i = 0; i < n; i++)
				printf("%s\n",A[i]);
				
	

			getch();

}

