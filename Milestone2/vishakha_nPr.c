//nPr - Permutations

#include<stdio.h>
#include<math.h>

int fact(int n);
 
int main()
{
    int n,r,npr;
	printf("Enter a number n: ");
  	scanf("%d",&n);
    printf("Enter a number r: ");
  	scanf("%d",&r);
  	
	npr=fact(n)/fact(n-r);
    printf("Value of %dP%d (nPr)= %d\n",n,r,npr);
}
 
int fact(int n)
{
    int i,f = 1;
    for(i=1 ; i<=n ; i++)
    {
        f = f * i;
    }
    return f;
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_nPr.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter a number n: 6
Enter a number r: 2
Value of 6P2 (nPr)= 30

C:\Users\Vishnu\Desktop\c>


*/