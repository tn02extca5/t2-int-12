//A program to convert weight entered in pounds(lbs) to kilogram(kg)
#include<stdio.h>

void main ()
{
	float lbs,kg;
	printf("Enter weight in lbs : \n");
	scanf("%f",&lbs);
	kg=lbs*0.4536;
	printf("weight in kg :%f kg \n", kg);
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_weight.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter weight in lbs :
45
weight in kg :20.412001 kg

*/