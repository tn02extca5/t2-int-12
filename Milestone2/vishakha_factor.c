//Function for finding and printing all factors of a number

#include<stdio.h>
#include<math.h>

int factor(int n);

int main ()
{
	int n;
	printf("Enter the number: ");
	scanf("%d", &n);
	
	factor(n);
}

int factor(int n)
{
	int i;
	printf("Factors of the given number (%d):\n",n);
	for(i=1; i <= n; ++i)
    {
        if (n%i == 0)
        {
			printf("%d  ",i);
        }
    }
	printf("\n");
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_factor.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter the number: 84
Factors of the given number (84):
1  2  3  4  6  7  12  14  21  28  42  84

*/