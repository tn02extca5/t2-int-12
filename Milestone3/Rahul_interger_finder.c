//Create a program to input a set of integers and then search the array to see if a particular integer exists in the array

#include<stdio.h>
#include<math.h>

int array[50];

void main ()
{
	int n,t,f=0;
	
	printf("Enter the number of elements in array[up to 50]: ");
	scanf("%d", &n);
	
	printf("Enter the %d elements of array:\n",n);
	for(int i=0; i<n; i++)
	{
		scanf("%d", &array[i]);
	}
	
	printf("Enter element to search: ");
	scanf("%d", &t);
	
	for(int i=0; i<n; i++)
	{
		if(array[i] == t)
		{
			f = 1;
			break;
		}
	}
	if(f==1)
	{
		printf("%d is found in the array.\n",t);
	}
	else
	{
		printf("%d is not found in the array.\n",t);
	}
}

/*
OUTPUT:

c:\Mycode>gcc interger_finder.c

c:\Mycode>a.exe
Enter the number of elements in array[up to 50]: 5
Enter the 5 elements of array:
1
2
4
5
6
Enter element to search: 3
3 is not found in the array.

c:\Mycode>a.exe
Enter the number of elements in array[up to 50]: 5
Enter the 5 elements of array:
1
0
8
9
4
Enter element to search: 4
4 is found in the array.

c:\Mycode>

*/