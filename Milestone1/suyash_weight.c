//A program to convert weight entered in pounds(lbs) to kilogram(kg)
#include<stdio.h>

void main ()
{
	float lbs,kg;
	printf("Enter weight in lbs : ");
	scanf("%f",&lbs);
	kg=lbs*0.4536;
	printf("weight in kg :%f kg ", kg);
}

/*
Output

C:\Users\Student\Desktop\c course>gcc weight.c

C:\Users\Student\Desktop\c course>a.exe
Enter weight in lbs : 25
weight in kg :11.340000 kg
C:\Users\Student\Desktop\c course>

*/