//double pow(double x, int n) - Calculates and returns x^n

#include<stdio.h>
#include<math.h>

double power(double x, int n);

int main()
{
	double x;
	int n;
	printf("Enter value of x: ");
	scanf(" %lf",&x);
	printf("Enter value of n: ");
	scanf(" %d",&n);
	
	double z = power(x,n);
	printf("Value of x^n: %lf\n",z);
	
	return 0;
}

double power(double x, int n)
{
	double z = pow(x,n);
	return z;
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_power.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter value of x: 4
Enter value of n: 3
Value of x^n: 64.000000


*/