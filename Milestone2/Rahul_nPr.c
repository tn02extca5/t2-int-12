//nPr - Permutations

#include<stdio.h>
#include<math.h>

int fact(int n);
 
int main()
{
    int n,r,npr;
	printf("Enter a number n: ");
  	scanf("%d",&n);
    printf("Enter a number r: ");
  	scanf("%d",&r);
  	
	npr=fact(n)/fact(n-r);
    printf("Value of %dP%d (nPr)= %d\n",n,r,npr);
}
 
int fact(int n)
{
    int i,f = 1;
    for(i=1 ; i<=n ; i++)
    {
        f = f * i;
    }
    return f;
}

/*
OUTPUT:

c:\Mycode>gcc npr.c

c:\Mycode>a.exe
Enter a number n: 6
Enter a number r: 3
Value of 6P3 (nPr)= 120

c:\Mycode>a.exe
Enter a number n: 9
Enter a number r: 5
Value of 9P5 (nPr)= 15120

c:\Mycode>

*/