//To find prime factors

#include<stdio.h>

void primefactor(int num);

void main ()
{
	int num;
	printf("\nEnter the number: ");
	scanf("%d",&num);
	
	primefactor(num);
	
}
void primefactor(int num)
{
	int q,i,j;
	printf("prime factors of the number  %d are: ",num);

	for(i=2; i<=num ; i++)
	{
		if (num%i==0)
		{
			q=1;
			for(j=2;j<=i/2;j++)
			{
				if(i%j==0)
				{
					q=0;
					break;
				}
				
			}
			if(q==1)
				printf("%d  ", i);
		}
			
	}
	
}

/*
Output:


C:\Suyash>gcc primefactor.c

C:\Suyash>a.exe

Enter the number: 90
prime factors of the number  90 are: 2  3  5
C:\Suyash>
*/