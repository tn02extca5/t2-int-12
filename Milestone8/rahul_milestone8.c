#include "headers.h"
#include "sorting_functions.c"

void mem_copy(void *dest,void *src,  int n);

int main(){

		int n,choice;
		
		printf("Enter no.of elements\n");

		if(scanf("%d",&n) == 0 ){
			printf("Not a number\n");
			goto  exit_condition;
		}else if(n == 0){
			printf("Cannot add zero elements !!\n");
			goto exit_condition;
		}


		do{
			system("cls");
			printf("MENU :: <1>INT <2>FLOAT <3>CHAR <4>STRING <5>QUIT \n");
			if(scanf("%d",&choice) == 0 ){
		
			printf("Not a number\n");
			goto  exit_condition;
		}
		switch(choice){
			case 1:
				int_sorting(n);
				break;
			case 2:
				float_sorting(n);
				break;
			case 3:
				char_sorting(n);
				break;
			case 4:
				string_sorting(n);
				break;
			case 5:
				goto exit_condition;

			default:
				printf("Invalid choice , please try again !!\n");
				getch();
		}
		}while(1);

		exit_condition:printf("Exiting the program .\n");
		return 0;
	}