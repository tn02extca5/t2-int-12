//Even & Odd

#include<stdio.h>
#include<conio.h>
int evenodd(int num);
void main ()
{
	int num;
	printf("Enter a number : ");
	scanf(" %d",&num);
	evenodd(num);
}	
int evenodd(int num)
{
	if (num%2==0)
		printf("number is even");
	else
		printf("number is odd");

}
/*
Output:


C:\Suyash>gcc evenodd.c

C:\Suyash>a.exe
Enter a number : 17
number is odd
C:\Suyash>a.exe
Enter a number : 80
number is even
C:\Suyash>

*/