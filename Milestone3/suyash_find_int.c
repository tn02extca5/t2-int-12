//Create a program to input a set of integers and then search the array to see if a particular integer exists in the array

#include<stdio.h>
#include<math.h>

int array[50];

void main ()
{
	int n,s,f=0;
	
	printf("Enter the number of elements in array[up to 50]: ");
	scanf("%d", &n);
	
	printf("Enter the %d elements of array:\n",n);
	for(int i=0; i<n; i++)
	{
		scanf("%d", &array[i]);
	}
	
	printf("Enter element to search: ");
	scanf("%d", &s);
	
	for(int i=0; i<n; i++)
	{
		if(array[i] == s)
		{
			f = 1;
			break;
		}
	}
	if(f==1)
	{
		printf("%d is found in the array.\n",s);
	}
	else
	{
		printf("%d is not found in the array.\n",s);
	}
}

/*
OUTPUT:

*/