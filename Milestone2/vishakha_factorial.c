//unsigned int fact(unsigned int n) - Calculates and returns the factorial of n

#include<stdio.h>
#include<math.h>

unsigned int fact(unsigned int n);

int main ()
{
	unsigned int n;
	printf("Enter the positive number: ");
	scanf("%u",&n);
	
	unsigned int f = fact(n);
	
	printf("Factorial of %u = %u\n",n,f);
	
}

unsigned int fact(unsigned int n)
{
	unsigned long long  fa = 1;
	
	for (int i=1; i<=n; ++i)
	{
		fa *= i;
	}
	return fa;
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_factorial.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter the positive number: 4
Factorial of 4 = 24

*/