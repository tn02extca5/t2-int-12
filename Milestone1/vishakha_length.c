//A program to convert length entered in inches to centimeters(cm)

#include<stdio.h> 

void main ()
{
	float i,c;
	printf("Enter length in inches : \n");
	scanf("%f",&i);
	c=2.54*i;
	printf("length in centimeter :%f cm ", c);
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_length.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter length in inches :
7
length in centimeter :17.780001 cm

*/