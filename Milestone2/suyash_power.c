// To find x^n

#include<stdio.h>
#include<conio.h>
#include<math.h>
double power(double x,int n);
void main ()
{
	double x;
	int n;
	printf("Enter a number : ");
	scanf(" %lf",&x);
	
	printf("Enter power : ");
	scanf(" %d",&n);
	
	double ans=power(x,n);
	printf("answer is %lf  : ", ans);
}	
double power(double x,int n)
{
	double ans;
	ans=pow(x,n);
	return ans;
}

/*
Output:


C:\Suyash>gcc power.c

C:\Suyash>a.exe
Enter a number : 2
Enter power : 2
answer is 4.000000  :
C:\Suyash>

*/