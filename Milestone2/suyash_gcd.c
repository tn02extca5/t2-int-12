//int gcd(int a, int b) - Calculates and returns the Greatest Common divisor of a and b

#include<stdio.h>
#include<math.h>

int gcd(int a, int b);

int main()
{
	int a,b;
	printf("Enter value of a: ");
	scanf(" %d",&a);
	printf("Enter value of b: ");
	scanf(" %d",&b);
	
	int c = gcd(a,b);
	printf("GCD value: %d\n",c);
	
	return 0;
}

int gcd(int a, int b)
{
	while(a!=b)
    {
        if(a > b)
            a = a-b;
        else
            b = b-a;
	}
	return a;
}

/*
OUTPUT:



*/