//A program to convert length entered in inches to centimeters(cm)

#include<stdio.h> 

void main ()
{
	float i,c;
	printf("Enter length in inches : ");
	scanf("%f",&i);
	c=2.54*i;
	printf("length in centimeter :%f cm ", c);
}

/*
Output

C:\Users\Student\Desktop\c course>gcc length.c

C:\Users\Student\Desktop\c course>a.exe
Enter length in inches : 5
length in centimeter :12.700000 cm
C:\Users\Student\Desktop\c course>

*/