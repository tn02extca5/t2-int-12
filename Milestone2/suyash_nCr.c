//nCr - Combinations

#include<stdio.h>
#include<math.h>

int fact(int n);
 
int main()
{
	int n,r,ncr;
	printf("Enter a number n : ");
  	scanf(" %d",&n);
	printf("Enter a number r : ");
  	scanf(" %d",&r);
	
  	ncr=fact(n)/(fact(r)*fact(n-r));
    printf("Value of %dC%d (nCr)= %d\n",n,r,ncr);
}
 
int fact(int n)
{
    int i,f = 1;
    for(i=1 ; i<=n ; i++)
    {
        f *=i;
    }
    return f;
}

/*
OUTPUT:


*/