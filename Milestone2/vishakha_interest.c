/* double FV(double rate, unsigned int nperiods, double PV) - 
Calculates and returns the Future Value of an investment based 
on the compound interest formula FV = PV * (1+rate)nperiods */

#include<stdio.h>
#include<math.h>

double FV(double rate, unsigned int nperiods, double PV);

int main ()
{
	double rate,PV;
	unsigned int nperiods;
	
	printf("Enter the Rate: ");
	scanf("%lf",&rate);

	printf("Enter the nperiods: ");
	scanf("%u",&nperiods);
	
	printf("Enter the PV: ");
	scanf("%lf",&PV);
	
	double a = FV(rate,nperiods,PV);
	
	printf("Investment Value: %lf\n",a);
	
}

double FV(double rate, unsigned int nperiods, double PV)
{
	double b = 1 + rate;
	double FV = PV * pow(b,nperiods);
	return FV;
}


/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_interest.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter the Rate: 40
Enter the nperiods: 3
Enter the PV: 20
Investment Value: 1378420.000000






*/