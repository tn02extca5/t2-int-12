
#include "node_structure.h"


void sort(struct Node *list);


struct Node*  push(struct Node **pointerToHead,int value){
	struct Node *temp1;
	struct Node *new_node = (struct Node*)malloc(sizeof(struct Node));


	//Empty List check and addition of first node;
	if(*pointerToHead == NULL){
		new_node->data = value;
		new_node->next = *pointerToHead;
		*pointerToHead = new_node;
	return *pointerToHead ;
	}
	else{

		temp1 = *pointerToHead;
		
		while(temp1->next != NULL) //temp->next is used since we want to stay at the last node !!
			temp1 = temp1->next;
	
		new_node->data = value;
		new_node->next = temp1->next;
		temp1->next = new_node;
	}
	

		return *pointerToHead;
}

int lengthOfStruct(struct Node *head){
	struct Node *temp = head;
int i = 0;
while(temp != NULL){
		i++;
		temp = temp->next;
	}

	return i;

}





void print(struct Node *head){
	struct Node *temp = head;

	if(temp == NULL){
		printf("Linked List is empty !!");
		return;
	}else{

	while(temp != NULL){
		printf("%d ", temp->data);
		temp = temp->next;
	}

}
}

//++++++++++++++++++++++++++++++++++
int count(struct Node *head,int num){
	struct Node *temp = head;
	int cnt = 0;

	while(temp != NULL){
		if(temp->data == num){
			cnt++;
		}

		temp = temp->next;
	}

	return cnt;

}

//++++++++++++++++++++++++++++++++++
int GetNth(struct Node *head,int index){
	struct Node *temp = head;
	int cnt = 0;

	
		while(temp != NULL){
		if(cnt == index){
			return temp->data;
		}

		cnt++;
		temp = temp->next;
	

	}

	return -1;
}
//++++++++++++++++++++++++++++++++++
void DeleteList(struct Node **pointerToHead){
	struct Node *temp,*temp1;
	temp = *pointerToHead;


	while(temp != NULL){ 
		temp1 = temp;
		temp = temp->next;
		free(temp1);
		
}

*pointerToHead = NULL;//free is called only on nodes and not on head pointer cause it just points to the first node and its memory isn't dynamically allocated.
	
}
//++++++++++++++++++++++++++++++++++
int pop(struct Node **pointerToHead){
	struct Node *temp;
	int data;

	if(*pointerToHead == NULL){
		printf("List is empty cannot pop any elements !!\n");
		return -1;
	}else{
		temp = *pointerToHead;
	
		*pointerToHead = temp->next;
		data = temp->data;
	
		free(temp);
	
		return data;}
}
//++++++++++++++++++++++++++++++++++

void InsertNth(struct Node **pointerToHead,int element, int position){
	struct Node *temp;
	struct Node *new_node = (struct Node*)malloc(sizeof(struct Node));
	temp = *pointerToHead;
	new_node->data = element;

if(*pointerToHead == NULL){
	if(position > 0){
		printf("Note =\n");
		return;
	}}

if(position == 0){
	new_node->next = temp;
	*pointerToHead = new_node;
}else if(position < 0 || position > lengthOfStruct(*pointerToHead)-1){
	printf("Error !!\n");
	printf("Press any key to continue !\n");
	getch();
	return;

}
else{
	for (int i = 0; i < position-1; ++i)
{
	temp = temp->next;

}

new_node->next =temp->next;
temp->next = new_node;}
	
}
//++++++++++++++++++++++++++++++++++
void SortedInsert(struct Node **pointerToHead,struct Node *new_node){
	struct Node *current,*prev = NULL;
	current = *pointerToHead;


	if(current == NULL){//Empty List
		new_node->next = NULL;
		*pointerToHead = new_node;
			return;
	}


	if(prev == NULL && (new_node->data <= current->data) ){ // logic for the first positon !
		new_node->next = current;
			*pointerToHead = new_node;
			return;
	}


	while(current != NULL){

		if(new_node->data <= current->data ){
			new_node->next = prev->next;
			prev->next = new_node;
			break; //for the first occucrnce of the greater number .

		}
		prev = current;
		current = current->next;
	}



	if(prev->next == NULL){  //logic for the last position !
		prev->next = new_node;
		new_node->next = NULL;
	}
}
//++++++++++++++++++++++++++++++++++
void InsertSort(struct Node **pointerToHead){

	struct Node *temp = *pointerToHead,*new_List = NULL;


	while(temp != NULL){
	struct Node *next = temp->next;	 //stores the address of the next node so it doesn't vanishes after sortedInsert !!
	SortedInsert(&new_List,temp);
	
	temp = next;
				
	}	

	*pointerToHead = new_List;
}
//++++++++++++++++++++++++++++++++++
void Append(struct Node **pointerToHead1,struct Node **pointerToHead2){

	struct Node *temp1;
	temp1 = *pointerToHead1;
	


	if(temp1 == NULL){
		*pointerToHead1 = *pointerToHead2;
		return;
	}else{
		while(temp1->next != NULL)
			temp1 = temp1->next;
	
		temp1->next = *pointerToHead2;
	
		*pointerToHead2 = NULL;}

}
//++++++++++++++++++++++++++++++++++
void FrontBackSplit(struct Node *source,struct Node **frontRef,struct Node **backRef){

	struct Node *temp = source;
	*frontRef = source;
	int cnt = 1;
	int len = lengthOfStruct(temp);
	if(len == 1){
		printf("\nList contains only one element,so cannot split the list any further !!\n");
		return;
	}

	if(len%2 == 0){ // for even no.of elements
		while(temp != NULL){
			if((len/cnt) == 2 && (len%cnt) == 0){
				*backRef = temp->next;
				temp->next = NULL;

			printf("\nFirst list :: \n");
			print(*frontRef);
			printf("\nSecond list is :: \n");
	
			print(*backRef);
				return;
			}
			cnt++;
			temp = temp->next;
		}
										
	}else{
		while(temp != NULL){
			if((len-1)/cnt == 2 && (len-1)%cnt == 0){
				temp = temp->next;
				*backRef = temp->next;
				temp->next = NULL;

			printf("\nFirst list :: \n");
			print(*frontRef);
			printf("\nSecond list is :: \n");
	
			print(*backRef);
				return;
			}
			cnt++;
			temp = temp->next;
		}

	}



}
//++++++++++++++++++++++++++++++++++
void RemoveDuplicates(struct Node *head){

	struct Node *prev = head,*temp = prev->next;

	while(temp != NULL){

		if(temp->data == prev->data){

			prev->next = temp->next;
			temp->next = NULL;
			free(temp);
			temp = prev;
		}

		prev = temp;
		temp = temp->next;
	}


}
//++++++++++++++++++++++++++++++++++
void MoveNode(struct Node **destRef,struct Node **srcRef){
	struct Node *temp = *srcRef;
	*srcRef = (*srcRef)->next;

	temp->next = NULL;
	temp->next = *destRef;
	*destRef = temp;

}
//++++++++++++++++++++++++++++++++++
void AlternatingSplit(struct Node *source,struct Node **aref,struct Node **bref){

	while(source != NULL){
		MoveNode(aref,&source);
		if(source != NULL){
			MoveNode(bref,&source);
		}
	}


}
//++++++++++++++++++++++++++++++++++
struct Node* ShuffleMerge(struct Node *a,struct Node *b){
	struct Node *new_List = NULL;
	int cnt = 0;
		
	while(a != NULL && b != NULL){
		if(cnt % 2 == 0)
				{MoveNode(&new_List,&a);}
				else
				{MoveNode(&new_List,&b);}

			cnt++;
		}

		if(a == NULL){while(b != NULL){
							MoveNode(&new_List,&b);
						}}

		if(b == NULL){while(a != NULL){
							MoveNode(&new_List,&a);}}

		return new_List;		
}
//++++++++++++++++++++++++++++++++++
struct Node* SortedMerge(struct Node *a,struct Node *b){

struct Node *new_List = NULL;
while(a != NULL && b != NULL){

	if(a->data >= b->data)
		MoveNode(&new_List,&b);
	else
		MoveNode(&new_List,&a);
}

if(a == NULL){
	while(b != NULL){
		MoveNode(&new_List,&b);
	}
}

if(b == NULL){
	while(a != NULL){
		MoveNode(&new_List,&a);
	}

}

return new_List;

}
//++++++++++++++++++++++++++++++++++
void MergeSort(struct Node *headRef){

	struct Node *a = NULL,*b = NULL;
	struct Node *new_List = headRef,*temp1 = NULL;
		printf("in mergersort\n");
	print(headRef);
	printf("+++++\n");
	FrontBackSplit(new_List,&a,&b);
	sort(a);
	sort(b);

	temp1 = SortedMerge(a,b);
    b = NULL;
	while(temp1 != NULL){
			MoveNode(&b,&temp1);
		} //reverses the list

		headRef = b;
		printf("\nSorted List ::\n");
		print(headRef);
}
//++++++++++++++++++++++++++++++++++
void sort(struct Node *list){
	struct Node *temp1 = NULL,*temp2 = NULL;
	int temp_var;
	temp1 = list;
	while(temp1 != NULL){
										temp2 = temp1->next;

										while(temp2 != NULL){

											if(temp1->data > temp2->data){
												
												temp_var = temp2->data;
												temp2->data = temp1->data;
												temp1->data = temp_var;

											}

											temp2 = temp2->next;
										}

										temp1 = temp1->next;
									}
}
//++++++++++++++++++++++++++++++++++
struct  Node* SortedIntersect(struct Node *a,struct Node *b){

	struct Node *temp1 = a,*temp2 = b;
	struct Node Dummy;
	Dummy.next = NULL;
	struct Node *new_List = &Dummy;


	while(temp1 != NULL && temp2 != NULL){

		if(temp1->data > temp2->data){
			temp2 = temp2->next;
		}else if(temp1->data == temp2->data){
			push((&new_List->next),temp1->data);
			new_List = new_List->next;
			temp1 = temp1->next;
			temp2 = temp2->next;
		}else if(temp1->data < temp2->data){
			temp1 = temp1->next;
		}
	}
	return Dummy.next;
	
};
//++++++++++++++++++++++++++++++++++
void Reverse(struct Node **pointerToHead){
			
	struct Node *b = NULL,*temp = NULL;	
	temp = *pointerToHead;	
			while(temp != NULL){
					MoveNode(&b,&temp);
								} 


		*pointerToHead = b;
}
//++++++++++++++++++++++++++++++++++

