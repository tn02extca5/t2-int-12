//Program to calculate the Area of Triangle

#include<stdio.h>

void main()
{
	float area,b,h;
	printf("Enter the base: \n");
	scanf("%f",&b);
	printf("Enter the height: \n");
	scanf("%f",&h);
	area=(b*h)/2;
	printf("Area of triangle is %f " , area );
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_triangle.c

C:\Users\Vishnu\Desktop\c>a.exe
Enter the base:
3
Enter the height:
6
Area of triangle is 9.000000
C:\Users\Vishnu\Desktop\c>





*/