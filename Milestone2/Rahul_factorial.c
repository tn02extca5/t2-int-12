//unsigned int fact(unsigned int n) - Calculates and returns the factorial of n

#include<stdio.h>
#include<math.h>

unsigned int fact(unsigned int n);

int main ()
{
	unsigned int n;
	printf("Enter the positive number: ");
	scanf("%u",&n);
	
	unsigned int f = fact(n);
	
	printf("Factorial of %u = %u\n",n,f);
	
}

unsigned int fact(unsigned int n)
{
	unsigned long long  fa = 1;
	
	for (int i=1; i<=n; ++i)
	{
		fa *= i;
	}
	return fa;
}

/*
OUTPUT:

c:\Mycode>gcc factorial.c

c:\Mycode>a.exe
Enter the positive number: 5
Factorial of 5 = 120

c:\Mycode>

*/