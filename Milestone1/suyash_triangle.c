//A program to calculate the Area of a Triangle

#include<stdio.h>

void main ()
{
	
	float b,h,area;
	printf("Enter base in meter:");
	scanf("%f", &b);
	printf("Enter height in meter:");
	scanf("%f", &h);
	area=(b*h)/2;
	printf("area of triangle is %f sq.meter" , area );
	
}

/*
OUTPUT:

C:\Users\Student\Desktop\c course>a.exe
Enter base in meter:10
Enter height in meter:20
area of triangle is 100.000000 sq.meter
C:\Users\Student\Desktop\c course>

*/