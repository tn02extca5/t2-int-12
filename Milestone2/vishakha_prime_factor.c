//Function for finding and printing all prime factors of a number

#include<stdio.h>

void prime_factor(int n);

void main ()
{
	int n;
	printf("\nEnter the number: ");
	scanf("%d",&n);
	
	prime_factor(n);
	
}

void prime_factor(int n)
{
	int p,i,j;
	printf("Prime factor of %d are:\n",n);
	
	for(i=2; i<=n; i++)
	{
		if(n%i == 0)
		{
			p = 1;
			for(j=2; j<=i/2; j++)
			{
				if(i%j == 0)
				{
					p = 0;
					break;
				}
			}
			if(p == 1)
			{
				printf("%d  ",i);
			}
		}
	}
	printf("\n");
}

/*
OUTPUT:
C:\Users\Vishnu\Desktop\c>gcc vishakha_prime_factor.c

C:\Users\Vishnu\Desktop\c>a.exe

Enter the number: 2541
Prime factor of 2541 are:
3  7  11
*/