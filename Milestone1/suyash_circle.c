//A program to calculate the Area of a Circle

#include<stdio.h>

void main ()
{
	
	float r,pi,area;
	pi=3.142;
	printf("Enter radius in meter:");
	scanf("%f", &r);
	area= r*r*pi;
	printf("area of circle is %0.2f sq.meter" , area );
	
}

/*
OUTPUT:

C:\Users\Student\Desktop\c course>gcc circle.c

C:\Users\Student\Desktop\c course>a.exe
Enter radius in meter:52
area of circle is 8495.97 sq.meter
C:\Users\Student\Desktop\c course>

*/