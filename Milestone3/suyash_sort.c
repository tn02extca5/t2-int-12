//Create a program to input a set of integers and sort them in ascending or descending order depending on users choice

#include<stdio.h>

void ascend(int x);
void descend(int x);

int array[50];

void main ()
{
	int n;
	char c;
	printf("Enter the number of elements in array[up to 50]: ");
	scanf("%d", &n);
	
	printf("Enter the %d elements of array:\n",n);
	for(int i=0; i<n; i++)
	{
		scanf("%d", &array[i]);
	}
	
	printf("Sort as Ascending or Descending order [A/D]: ");
	scanf(" %c", &c);
	
	if (c == 'A')
	{
		ascend(n);
	}
	else if (c == 'D')
	{
		descend(n);
	}
	else
	{
		printf("Please write the valid order command!\n");
	}
}

void ascend(int n)
{
	int i , j, k;
	for ( i = 1; i <=n-1 ; i++)
	{
		for ( j = 1; j <=n-i; j++)
		{
			if(array[j-1] >= array[j])
			{
				k = array[j-1];
				array[j-1] = array[j];
				array[j] = k;
			}
			
		}
	}
	printf("Array of Elemenst in Ascending Order are:\n");
	for (int i = 0; i < n; i++)
	{
		printf("%d  ", array[i]);
	}
	printf("\n");
}

void descend(int n)
{
	int i , j, k;
	for ( i =1 ; i <= n-1; i++)
	{
		for ( j = 1 ; j <=n-i; j++)
		{
			if(array[j-1] <= array[j])
			{
				k = array[j-1];
				array[j-1] = array[j];
				array[j] = k;
			}
			
		}
	}
	printf("Array of Elemenst in Descending Order are:\n");
	for (int i = 0; i < n; i++)
	{
		printf("%d  ", array[i]);
	}
	printf("\n");
}

/*
OUTPUT:


*/